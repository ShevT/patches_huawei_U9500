#!/bin/bash

cd `dirname $0`
DSTDIR=$1

if [ -z "$DSTDIR" ]
then
    echo "Usage: $0 <sources dir>"
    exit 1
fi

red=$(tput setaf 1) # Red
grn=$(tput setaf 2) # Green
txtrst=$(tput sgr0) # Reset

# back port bluez
echo ""

DISTR=`head -1 $DSTDIR/.repo/manifests/README.*`

case "$DISTR" in

  "CyanogenMod")
    echo "${grn}---=== CyanogenMod ===---${txtrst}"

    # Show Network Speed patch
    echo ""
    echo "${grn}Applying \"Show Network Speed\" patch (by realmenvvs 4pda)${txtrst}"
    cp  allpatches/traffic/Traffic.java $DSTDIR/frameworks/base/packages/SystemUI/src/com/android/systemui/statusbar/policy
    cat allpatches/traffic/Traffic_frameworks_base_core.patch | patch -d $DSTDIR/frameworks/base -p1 -N -r -
    cat allpatches/traffic/Traffic_Settings.patch | patch -d $DSTDIR/packages/apps/Settings -p1 -N -r -

    # Allowed to provide the name CM_BUILDTYPE
    echo ""
    echo "${grn}Allowed to provide the name CM_BUILDTYPE${txtrst}"
    cat allpatches/CM_BUILDTYPE.patch | patch -d $DSTDIR/vendor/cm -p1 -N -r -

    # Allow to make their BUILD_DISPLAY_ID
    echo ""
    echo "${grn}Applying BUILD_DISPLAY_ID patch${txtrst}"
    cat allpatches/BUILD_DISPLAY_ID.patch | patch -d $DSTDIR/build -p1 -N -r -
    ;;

  "MoKee OpenSource")
    echo "${grn}---=== MoKee OpenSource ===---${txtrst}"
    ;;

  *)
    echo "${red}*================== Error!!! ================*"
    echo "| Who is here? I do not know what the system |"
    echo "*============================================*${txtrst}"
    exit 1
  ;;
esac

    # Fix Terminal
    echo ""
    echo "${grn}Applying vendor CM patch. Fix Terminal${txtrst}"
    cat allpatches/vendor_cm.patch | patch -d $DSTDIR/vendor/cm -p1 -N -r -

# Airplane
echo ""
echo "${grn}Applying Airplane patch${txtrst}"
cat allpatches/Airplane.patch | patch -d $DSTDIR/packages/apps/Phone/ -p1 -N -r -

# AudioRecord patch
echo ""
echo "${grn}Applying AudioRecord patch${txtrst}"
cat allpatches/AudioRecord.patch | patch -d $DSTDIR/frameworks/av/ -p1 -N -r -

# Adding caller geo info database
echo ""
echo "${grn}Adding CallerGeoInfo data${txtrst}"
cp allpatches/geoloc/86_zh $DSTDIR/external/libphonenumber/java/src/com/android/i18n/phonenumbers/geocoding/data/86_zh
cp allpatches/geoloc/PhoneNumberMetadataProto_CN $DSTDIR/external/libphonenumber/java/src/com/android/i18n/phonenumbers/data/PhoneNumberMetadataProto_CN

# Camera patch
echo ""
echo "${grn}Applying Camera patch${txtrst}"
cat allpatches/Camera.patch | patch -d $DSTDIR/packages/apps/Camera -p1 -N -r -

# WiFi Country patch
echo ""
echo "${grn}Applying WiFi Country patch${txtrst}"
cat allpatches/WiFi_Country.patch | patch -d $DSTDIR/frameworks/opt/telephony -p1 -N -r -

# Ramdisk patch
echo ""
echo "${grn}Applying Ramdisk patch${txtrst}"
cat allpatches/ramdisk.patch | patch -d $DSTDIR/system/core -p1 -N -r -

# QUICKSETTINGS WITH 4 TILES COLOUMS
echo ""
echo "${grn}QUICKSETTINGS WITH 4 TILES COLOUMS patch${txtrst}"
cat allpatches/quicksettings_with_4_tiles_columns.patch | patch -d $DSTDIR/frameworks/base -p1 -N -r -

# if not removed - there will be errors
echo ""
echo "${grn}Remove *.orig files${txtrst}"
find $DSTDIR -name "*.orig" -delete

# Change sounds
echo ""
echo "${grn}Change sounds${txtrst}"
cat allpatches/media.patch | patch -d $DSTDIR/vendor/cm -p1 -N -r -

# linaro
echo ""
echo "${grn}Copy gcc${txtrst}"
cp -r linaro-4.8 $DSTDIR/prebuilts/gcc/linux-x86/arm/

echo "${grn}Apply gcc patches${txtrst}"
echo ""
cat allpatches/gcc/build_linaro.4.8.patch | patch -d $DSTDIR/build -p1 -N -r -
cat allpatches/gcc/01_bluedroid.patch | patch -d $DSTDIR/external/bluetooth/bluedroid -p1 -N -r -
cat allpatches/gcc/01_bluedroid_bta.patch | patch -d $DSTDIR/external/bluetooth/bluedroid -p1 -N -r -
cat allpatches/gcc/02_system_core_libcorkscrew.patch | patch -d $DSTDIR/system/core -p1 -N -r -
cat allpatches/gcc/03_system_security_keystore.patch | patch -d $DSTDIR/system/security -p1 -N -r -
cat allpatches/gcc/04_external_oprofile.patch | patch -d $DSTDIR/external/oprofile -p1 -N -r -
cat allpatches/gcc/04_external_oprofile1.patch | patch -d $DSTDIR/external/oprofile -p1 -N -r -
cat allpatches/gcc/05_frameworks_rs.patch | patch -d $DSTDIR/frameworks/rs -p1 -N -r -
cat allpatches/gcc/06_android_external_ping6.patch | patch -d $DSTDIR/external/ping6 -p1 -N -r -
cat allpatches/gcc/07_android_external_ping.patch | patch -d $DSTDIR/external/ping -p1 -N -r -
cat allpatches/gcc/08_android_external_v8.patch | patch -d $DSTDIR/external/v8 -p1 -N -r -
echo ""
echo "${grn}Done${txtrst}"
